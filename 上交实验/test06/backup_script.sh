#!/bin/bash

# 定义备份目录路径
BACKUP_DIR=/path/to/backup

# 定义备份文件名
BACKUP_FILE=database_backup_$(date +%Y%m%d).dmp

# 导出数据库
expdp system/password DIRECTORY=backup_dir DUMPFILE=$BACKUP_FILE FULL=y

# 将备份文件移动至备份目录
mv $BACKUP_FILE $BACKUP_DIR/

# 记录备份记录
echo "$(date +%Y-%m-%d_%H:%M:%S) Backup Success" >> /path/to/logfile
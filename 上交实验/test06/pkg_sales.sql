CREATE OR REPLACE PACKAGE pkg_sales AS
  -- 存储过程：计算订单金额
  PROCEDURE CalcOrderAmount(pOrderId IN INT, oAmount OUT NUMBER);

  -- 存储过程：更新库存
  PROCEDURE UpdateInventory(pProductId IN INT, pDeltaStock IN INT, pSalePrice IN NUMBER);

  -- 存储过程：按日期查询订单
  PROCEDURE QueryOrderByDate(pStartDate IN DATE, pEndDate IN DATE, pResult OUT SYS_REFCURSOR);

  -- 函数：查询商品平均价格
  FUNCTION GetAvgPrice(pProductId IN INT) RETURN NUMBER;
END pkg_sales;
/

CREATE OR REPLACE PACKAGE BODY pkg_sales AS
  -- 存储过程：计算订单金额
  PROCEDURE CalcOrderAmount(pOrderId IN INT, oAmount OUT NUMBER) IS
    vTotal NUMBER;
  BEGIN
    SELECT SUM(Quantity * UnitPrice) INTO vTotal FROM OrderDetail WHERE OId = pOrderId;
    oAmount := vTotal;
  END;

  -- 存储过程：更新库存
  PROCEDURE UpdateInventory(pProductId IN INT, pDeltaStock IN INT, pSalePrice IN NUMBER) IS
  BEGIN
    UPDATE Inventory SET Stock = Stock + pDeltaStock, SalePrice = pSalePrice WHERE PId = pProductId;
  END;

  -- 存储过程：按日期查询订单
  PROCEDURE QueryOrderByDate(pStartDate IN DATE, pEndDate IN DATE, pResult OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN pResult FOR SELECT * FROM `Order` WHERE ODate >= pStartDate AND ODate <= pEndDate;
  END;

  -- 函数：查询商品平均价格
  FUNCTION GetAvgPrice(pProductId IN INT) RETURN NUMBER IS
    vAvgPrice NUMBER;
  BEGIN
    SELECT AVG(UnitPrice) INTO vAvgPrice FROM OrderDetail WHERE PId = pProductId;
    RETURN vAvgPrice;
  END;
END pkg_sales;
/

## 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

### 一、表及表空间设计方案

#### 1.表空间设计

先创建两个表空间TBS1和TBS2来存储表的数据。

#### 2.表设计

创建4张表：

- 商品表（Product）：存储商品基本信息，包括商品编号、商品名称、商品类型、商品价格等字段。
- 库存表（Inventory）：存储各类商品的库存量、进货价、销售价、零售价、采购日期等字段。
- 订单表（Order）：存储客户订单信息，包括订单编号、客户名称、订单金额、订单日期等字段。
- 订单明细表（OrderDetail）：记录每个订单中的商品信息，包括商品编号、商品数量、商品单价等字段。

```
CREATE TABLESPACE TBS1 DATAFILE '/path/to/tbs1.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
CREATE TABLESPACE TBS2 DATAFILE '/path/to/tbs2.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;

-- 创建商品表
CREATE TABLE Product (
   PId INT PRIMARY KEY,
   PName VARCHAR2(50) NOT NULL,
   PType VARCHAR2(50),
   PPrice NUMBER(10,2)
) TABLESPACE TBS1;

-- 创建库存表
CREATE TABLE Inventory (
   IId INT PRIMARY KEY,
   PId INT REFERENCES Product(PId) ON DELETE CASCADE,
   Stock INT NOT NULL,
   PurchasePrice NUMBER(10,2) NOT NULL,
   SalePrice NUMBER(10,2),
   RetailPrice NUMBER(10,2),
   PurchaseDate DATE
) TABLESPACE TBS1;

-- 创建订单表
CREATE TABLE `Order` (
   OId INT PRIMARY KEY,
   CName VARCHAR2(50) NOT NULL,
   OAmount NUMBER(10,2) NOT NULL,
   ODate DATE
) TABLESPACE TBS2;

-- 创建订单明细表
CREATE TABLE OrderDetail (
   OId INT REFERENCES `Order`(OId) ON DELETE CASCADE,
   PId INT REFERENCES Product(PId),
   Quantity INT NOT NULL,
   UnitPrice NUMBER(10,2),
   PRIMARY KEY (OId, PId)
) TABLESPACE TBS2;
```



### 二、权限及用户分配方案

创建两个用户：一个是管理员用户（Admin），具有最高权限，另一个是普通用户（User），只能执行一部分查询操作。

- 管理员用户（Admin）：拥有所有表的操作权限，包括增、删、改、查等。同时，管理员用户对程序包也有执行权限。
- 普通用户（User）：只能进行查询操作，无法对数据进行修改，同时无权限访问程序包。

```

CREATE USER Admin IDENTIFIED BY password DEFAULT TABLESPACE TBS1 TEMPORARY TABLESPACE TEMP;
CREATE USER User IDENTIFIED BY password DEFAULT TABLESPACE TBS1 TEMPORARY TABLESPACE TEMP;

GRANT CREATE SESSION TO Admin, User;
GRANT CREATE TABLE TO Admin, User;
GRANT CREATE SEQUENCE TO Admin, User;
GRANT CREATE TRIGGER TO Admin, User;

GRANT DBA TO Admin; -- 授予管理员用户所有权限
```



### 三、程序包设计

创建一个程序包，其中包含了一些存储过程和函数，以实现复杂的业务逻辑。

#### 1.存储过程

- 计算订单金额（CalcOrderAmount）：输入订单编号，根据订单明细表中的相关信息计算订单总金额，并将结果返回。
- 更新库存（UpdateInventory）：输入商品编号、更新的库存数量等信息，更新库存表中的数据。
- 按日期查询订单（QueryOrderByDate）：输入起始日期和截止日期，查询该时间段内的订单信息并返回查询结果。

#### 2.函数

- 查询商品平均价格（GetAvgPrice）：输入商品编号，从库存表中查询该商品的所有成交价，并计算出平均价格，将结果返回。

```
CREATE OR REPLACE PACKAGE pkg_sales AS
  -- 存储过程：计算订单金额
  PROCEDURE CalcOrderAmount(pOrderId IN INT, oAmount OUT NUMBER);

  -- 存储过程：更新库存
  PROCEDURE UpdateInventory(pProductId IN INT, pDeltaStock IN INT, pSalePrice IN NUMBER);

  -- 存储过程：按日期查询订单
  PROCEDURE QueryOrderByDate(pStartDate IN DATE, pEndDate IN DATE, pResult OUT SYS_REFCURSOR);

  -- 函数：查询商品平均价格
  FUNCTION GetAvgPrice(pProductId IN INT) RETURN NUMBER;
END pkg_sales;
/

CREATE OR REPLACE PACKAGE BODY pkg_sales AS
  -- 存储过程：计算订单金额
  PROCEDURE CalcOrderAmount(pOrderId IN INT, oAmount OUT NUMBER) IS
    vTotal NUMBER;
  BEGIN
    SELECT SUM(Quantity * UnitPrice) INTO vTotal FROM OrderDetail WHERE OId = pOrderId;
    oAmount := vTotal;
  END;

  -- 存储过程：更新库存
  PROCEDURE UpdateInventory(pProductId IN INT, pDeltaStock IN INT, pSalePrice IN NUMBER) IS
  BEGIN
    UPDATE Inventory SET Stock = Stock + pDeltaStock, SalePrice = pSalePrice WHERE PId = pProductId;
  END;

  -- 存储过程：按日期查询订单
  PROCEDURE QueryOrderByDate(pStartDate IN DATE, pEndDate IN DATE, pResult OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN pResult FOR SELECT * FROM `Order` WHERE ODate >= pStartDate AND ODate <= pEndDate;
  END;

  -- 函数：查询商品平均价格
  FUNCTION GetAvgPrice(pProductId IN INT) RETURN NUMBER IS
    vAvgPrice NUMBER;
  BEGIN
    SELECT AVG(UnitPrice) INTO vAvgPrice FROM OrderDetail WHERE PId = pProductId;
    RETURN vAvgPrice;
  END;
END pkg_sales;
/
```



### 四、备份方案设计

为了保障数据的安全性，我使用定期备份方案。

```
#!/bin/bash

# 定义备份目录路径
BACKUP_DIR=/path/to/backup

# 定义备份文件名
BACKUP_FILE=database_backup_$(date +%Y%m%d).dmp

# 导出数据库
expdp system/password DIRECTORY=backup_dir DUMPFILE=$BACKUP_FILE FULL=y

# 将备份文件移动至备份目录
mv $BACKUP_FILE $BACKUP_DIR/

# 记录备份记录
echo "$(date +%Y-%m-%d_%H:%M:%S) Backup Success" >> /path/to/logfile
```


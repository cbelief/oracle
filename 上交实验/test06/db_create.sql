CREATE TABLESPACE TBS1 DATAFILE '/path/to/tbs1.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
CREATE TABLESPACE TBS2 DATAFILE '/path/to/tbs2.dbf' SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;

CREATE USER Admin IDENTIFIED BY password DEFAULT TABLESPACE TBS1 TEMPORARY TABLESPACE TEMP;
CREATE USER User IDENTIFIED BY password DEFAULT TABLESPACE TBS1 TEMPORARY TABLESPACE TEMP;

GRANT CREATE SESSION TO Admin, User;
GRANT CREATE TABLE TO Admin, User;
GRANT CREATE SEQUENCE TO Admin, User;
GRANT CREATE TRIGGER TO Admin, User;

GRANT DBA TO Admin; -- 授予管理员用户所有权限

-- 创建商品表
CREATE TABLE Product (
   PId INT PRIMARY KEY,
   PName VARCHAR2(50) NOT NULL,
   PType VARCHAR2(50),
   PPrice NUMBER(10,2)
) TABLESPACE TBS1;

-- 创建库存表
CREATE TABLE Inventory (
   IId INT PRIMARY KEY,
   PId INT REFERENCES Product(PId) ON DELETE CASCADE,
   Stock INT NOT NULL,
   PurchasePrice NUMBER(10,2) NOT NULL,
   SalePrice NUMBER(10,2),
   RetailPrice NUMBER(10,2),
   PurchaseDate DATE
) TABLESPACE TBS1;

-- 创建订单表
CREATE TABLE `Order` (
   OId INT PRIMARY KEY,
   CName VARCHAR2(50) NOT NULL,
   OAmount NUMBER(10,2) NOT NULL,
   ODate DATE
) TABLESPACE TBS2;

-- 创建订单明细表
CREATE TABLE OrderDetail (
   OId INT REFERENCES `Order`(OId) ON DELETE CASCADE,
   PId INT REFERENCES Product(PId),
   Quantity INT NOT NULL,
   UnitPrice NUMBER(10,2),
   PRIMARY KEY (OId, PId)
) TABLESPACE TBS2;
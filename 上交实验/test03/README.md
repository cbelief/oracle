# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

创建两张表：订单表(orders)与订单详表(order_details)。

两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

orders表按订单日期（order_date）设置范围分区。

order_details表设置引用分区。

表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。

写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。

进行分区与不分区的对比实验。

## 实验步骤

### 创建表ORDERS → 创建序列 → 创建触发器 → 创建序列的主索引建；

```
--------------------------------------------------------
--  DDL for Table ORDERS
--------------------------------------------------------

  CREATE TABLE "HR"."ORDERS" 
   (	"ORDER_ID" NUMBER(9,0), 
	"CUSTOMER_NAME" VARCHAR2(40 BYTE), 
	"CUSTOMER_TEL" VARCHAR2(40 BYTE), 
	"ORDER_DATE" DATE, 
	"EMPLOYEE_ID" NUMBER(6,0), 
	"DISCOUNT" NUMBER(8,2), 
	"TRADE_RECEIVABLE" NUMBER(8,2)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" 
  PARTITION BY RANGE ("ORDER_DATE") 
 (PARTITION "PARTITION2016"  VALUES LESS THAN (TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" , 
 PARTITION "PARTITION2020"  VALUES LESS THAN (TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" , 
 PARTITION "PARTITION2021"  VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" ) ;
--------------------------------------------------------
--  DDL for Index TABLE1_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."TABLE1_PK" ON "HR"."ORDERS" ("ORDER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Index ORDERS_INDEX1
--------------------------------------------------------

  CREATE INDEX "HR"."ORDERS_INDEX1" ON "HR"."ORDERS" ("CUSTOMER_NAME") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  DDL for Trigger ORDERS_ID_INCREASE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "HR"."ORDERS_ID_INCREASE" 
   before insert on "HR"."ORDERS" 
   for each row 
begin  
   if inserting then 
      if :NEW."ORDER_ID" is null then 
         select ORDERS_ORDER_ID.nextval into :NEW."ORDER_ID" from dual; 
      end if; 
   end if; 
end;

/
ALTER TRIGGER "HR"."ORDERS_ID_INCREASE" ENABLE;
--------------------------------------------------------
--  Constraints for Table ORDERS
--------------------------------------------------------

  ALTER TABLE "HR"."ORDERS" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDERS" MODIFY ("ORDER_DATE" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDERS" ADD CONSTRAINT "TABLE1_PK" PRIMARY KEY ("ORDER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX"  ENABLE;
```

--------------------------------------------------------
```
--  DDL for Sequence ORDERS_ORDER_ID
--------------------------------------------------------

   CREATE SEQUENCE  "HR"."ORDERS_ORDER_ID"  MINVALUE 1 MAXVALUE 9999999999999999 INCREMENT BY 1 START WITH 121 CACHE 20 ORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

### 创建表ORDER_DETAILS → 创建序列 → 创建触发器 → 创建序列的主索引建；

```
--------------------------------------------------------
--  DDL for Table ORDER_DETAILS
--------------------------------------------------------

  CREATE TABLE "HR"."ORDER_DETAILS" 
   (	"ID" NUMBER(9,0), 
	"ORDER_ID" NUMBER(9,0), 
	"PRODUCT_ID" VARCHAR2(40 BYTE), 
	"PRODUCT_NUM" NUMBER(8,2), 
	"PRODUCT_PRICE" NUMBER(8,2), 
	 CONSTRAINT "ORDER_DETAILS_FK1" FOREIGN KEY ("ORDER_ID")
	  REFERENCES "HR"."ORDERS" ("ORDER_ID") ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  PARTITION BY REFERENCE ("ORDER_DETAILS_FK1") 
 (PARTITION "PARTITION2016" SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" , 
 PARTITION "PARTITION2020" SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" , 
 PARTITION "PARTITION2021" SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSAUX" ) ;
--------------------------------------------------------
--  DDL for Index ORDER_DETAILS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "HR"."ORDER_DETAILS_PK" ON "HR"."ORDER_DETAILS" ("ORDER_ID", "PRODUCT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX" ;
--------------------------------------------------------
--  Constraints for Table ORDER_DETAILS
--------------------------------------------------------

  ALTER TABLE "HR"."ORDER_DETAILS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAILS" MODIFY ("ORDER_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAILS" MODIFY ("PRODUCT_ID" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAILS" MODIFY ("PRODUCT_NUM" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAILS" MODIFY ("PRODUCT_PRICE" NOT NULL ENABLE);
  ALTER TABLE "HR"."ORDER_DETAILS" ADD CONSTRAINT "ORDER_DETAILS_PK" PRIMARY KEY ("ORDER_ID", "PRODUCT_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "SYSAUX"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ORDER_DETAILS
--------------------------------------------------------

  ALTER TABLE "HR"."ORDER_DETAILS" ADD CONSTRAINT "ORDER_DETAILS_FK1" FOREIGN KEY ("ORDER_ID")
	  REFERENCES "HR"."ORDERS" ("ORDER_ID") ENABLE;
```

```
--------------------------------------------------------
--  DDL for Sequence ORDER_DETAILS_ID
--------------------------------------------------------

   CREATE SEQUENCE  "HR"."ORDER_DETAILS_ID"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 ORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```

### 给ORDERS表添加50万条数据



```
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<500000 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(orders_order_id.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/
```

## 给ORDER_DETAILS表添加200万条数据

```
declare 
   i integer;
   j integer;
BEGIN  
  i:=0;
  while i<40000 loop
    i:=i+1;
    j:=1;
    while j<5 loop
        j:=j+1;
        insert into order_details(id,order_id) 
        values(order_details_id.nextval,i);
     end loop;   
  end loop;
  commit;
END;
/
```

## 两个表的联合查询

```
select id from order o join oredr_details od on o.order_id = od.oredr_id whewr o.customer_name = 'xiaoming'

select id from order o join oredr_details od on o.order_id = od.oredr_id whewr o.customer_tel = '114514'
```

customer_name为索引，查找效率远高于不是索引的customer_tel

```
select id from order o join oredr_details od on o.order_id = od.oredr_id whewr o.order_data = <2020
```

orders表按订单日期（order_date）设置范围分区后查询效率远高于未通过order_data分区的order表

### 总结

按照父表分区的方式对子表进行分区，但子表没有相同的列，那么就可以使用引用分区。

序列(Sequence)是序列号生成器，可以为表中的行自动生成序列号，产生一组等间隔的数值(类型为数字)。不占用磁盘空间，占用内存。其主要用途是生成表的主键值，可以在插入语句中引用，也可以通过查询检查当前值，或使序列增至下一个值

分区通过将大表划分成多个小表，并在每个小表上建立本地索引可以大大缩小索引数据文件的大小，从而更快的定位到目标数据来提升访问性能。当一张表的数据量到达上百万乃至亿行的时候，分区能极大提高查询速度。

Oracle存储索引的数据结构是B树，位图索引也是如此，只不过是叶子节点不同。将那些经常作为条件来查询的关键字段定义为索引能极大地提高数据库查询速度。

